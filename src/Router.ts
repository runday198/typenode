import * as http from "http";

// TODO: Implement this class.
export class Router {
  private handlers: {
    [method: string]: {
      [path: string]: Handler<unknown>;
    };
  } = {};

  private register<T>(
    method: HTTPMethod,
    path: string,
    handler: Handler<T>
  ): this {
    if (this.handlers[method] && this.handlers[method][path]) {
      throw new Error(`Handler already exists for ${method} ${path}`);
    }

    if (!this.handlers[method]) {
      this.handlers[method] = {};
    }

    this.handlers[method][path] = handler as Handler;

    return this;
  }

  get<T>(path: string, handler: Handler<T>): this {
    return this.register(HTTPMethod.GET, path, handler);
  }

  post<T>(path: string, handler: Handler<T>): this {
    return this.register(HTTPMethod.POST, path, handler);
  }

  put<T>(path: string, handler: Handler<T>): this {
    return this.register(HTTPMethod.PUT, path, handler);
  }

  delete<T>(path: string, handler: Handler<T>): this {
    return this.register(HTTPMethod.DELETE, path, handler);
  }

  getHandler() {
    return async (
      request: http.IncomingMessage & {
        body: unknown;
        method: string;
        url: string;
      },
      response: http.ServerResponse
    ) => {
      try {
        const chunks = [];
        for await (const chunk of request) {
          chunks.push(chunk);
        }

        const body = Buffer.from(chunks.join(""));
        request.body = JSON.parse(body.toString());

        const methodHandlers = this.handlers[request.method];
        const handler = methodHandlers
          ? methodHandlers[request.url]
          : undefined;

        if (handler) {
          await handler(request, response);
        } else {
          response.statusCode = 404;
          response.end();
        }
      } catch (error) {
        console.error(error);
        response.statusCode = 500;
        response.end();
      }
    };
  }
}

export type Handler<T = unknown> = (
  request: http.IncomingMessage & { body: T },
  response: http.ServerResponse
) => Promise<void>;

export enum HTTPMethod {
  GET = "GET",
  POST = "POST",
  PUT = "PUT",
  DELETE = "DELETE",
}
